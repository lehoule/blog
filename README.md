# cantcode.dev

## Images
Logo: https://logomakr.com/5wSUCd
Icon: logomakr.com/46MpB2

## Development
* Open an issue and it's related branch via gitlab and checkout
* Open VSCode with remote containers and run:
```
npm install && npm run develop
```

Open up the blog via http://localhost:8000/

## Tests
You can run the tests with:
```
# For development
./node_modules/cypress/bin/cypress install
npm run develop
npm run cy:run

# For the full test run 
npm runtest:e2e:ci
```
