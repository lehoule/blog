describe("Index tests", () => {
  beforeEach(() => {
  });

  it("loads the tag page correctly", () => {
    cy.visit("/tags")
    cy.get("ul").children('li').should('have.length.gte', 2)
  });

  it("loads the about page correctly", () => {
    cy.visit("/about")
    cy.get("main").children("h2").contains("About")
  });

  it("contains the header", () => {
    cy.visit("/")
    cy.get('[title="Can\'t Code Logo"]').should('be.visible')
    cy.get("picture").children('img').should('be.visible')
  });

  it("contains a list of articles", () => {
    cy.visit("/")
    cy.get("section").children('article').should('have.length.gte', 2)
  })

  it("contains the footer", () => {
    cy.visit("/")
    cy.get("footer").should('be.visible')
  })

  it("load the rss feed", () => {
    cy.request("/rss.xml")
  })
});
