---
title: "Introduction"
date: "2020-04-21"  
tags: ["general", "infrastructure"]
---

Writing code is hard. Writing code that doesn't become a mess is even harder.

This blog is dedicated to help you make it a smaller mess, one line at a time.
